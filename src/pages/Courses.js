import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses(){
	//checks to see if the mocj data was captured
	console.log(coursesData);
	console.log(coursesData[0]);
	
	const courses = coursesData.map(course => {
		return (
			<CourseCard key={course.id} courseProps ={course} />
			)
	})

	return(
		<>
		{courses}
			{/* 

			{} - used in props to signify that we are providing information
			
			Props Drilling - we can pass information from one component to another using props

			*/}
			{/*<CourseCard courseProp ={coursesData[0]} />
			*/}
		</>
	)
}