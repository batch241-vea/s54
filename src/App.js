import './App.css';

import { useState } from 'react';

import { UserProvider } from './UserContext';

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import AppNavbar from './components/AppNavbar';

import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
// import NotFound from './components/NotFound';

import { Container } from 'react-bootstrap';

function App() {

  //This will be used to store user information that will be used for validating if a user is logged in on the app or not
  //State hook for the user state that  is defined for global state
  const [user, setUser] = useState({email: localStorage.getItem('email')});

  const unsetUser = () => {
    localStorage.clear();
  }

  return (
    /*Fragments - common pattern in React.js for a component to return multiple elements*/
    <>
    {/*Initializes that dynamic routing will be involved*/}
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>  
        <AppNavbar/>
        <Container>
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/courses" element={<Courses/>}/>
            <Route path="/register" element={<Register/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/*" element={<Error/>} />
          }

            {/*<Route path="*" element={<NotFound/>} />*/}
            {/*
            "*" - is a wildcard character that will match any path that was not declared in the previous routes.
            */}
          </Routes>

        </Container>
      </Router> 
    </UserProvider>
    </>
  );
}

export default App;
